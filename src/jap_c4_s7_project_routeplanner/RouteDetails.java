package jap_c4_s7_project_routeplanner;

import java.io.*;

public class RouteDetails {
	Route[] routes = new Route[recordCount()];
	int count;
	
	
	public int recordCount() {
		count=0;
		String line;
		try {
			BufferedReader br = new BufferedReader(new FileReader("routes.txt"));
			while ((line = br.readLine()) != null) {
				count+=1;
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	void storeRecords() {
		String line1;
		int i = 0;
		System.out.println();
		try {
			BufferedReader br = new BufferedReader(new FileReader("routes.txt"));
			while ((line1 = br.readLine()) != null)
			{
				//System.out.println(i);
				String arr[] = line1.split(",");
				routes[i] = new Route(arr[0], arr[1], arr[2], arr[3], arr[4]);
				i++;
				}
			
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	void showDirectFlights(String fromCity,String[] toCities)
	{
		System.out.println("==========DIRECT FLIGHTS==============");
		int c=0;
		count=0;
		try
		{
		for(int i=0;i<routes.length;i++)
		{
			for(String j:toCities)
			{
				if(routes[i].getTo().equals(j) && routes[i].getFrom().equals(fromCity))
				{
					System.out.println(routes[i]);
					c++;
					count++;
				}
			}
		}
		if(c==0)
		{
			System.out.println("We are sorry");
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	void sortDirectFlights(String directFlights[],String fromCity)
	{
		System.out.println("==========SORTED DIRECT FLIGHTS==============");
		Route rou[] = new Route[count];
		Route temp;
		int c=0;
		for(int i=0;i<routes.length;i++)
		{
			for(String j:directFlights)
			{
				if(routes[i].getTo().equals(j) && routes[i].getFrom().equals(fromCity))
				{
					rou[c] = routes[i];
					c++;
				}
			}
		}
		for(int i=0;i<rou.length-1;i++)
		{
			char first = rou[i].getTo().charAt(0);
			char second = rou[i+1].getTo().charAt(0);
			if((int)first>(int)second)
			{
				temp = rou[i];
				rou[i]=rou[i+1];
				rou[i+1]=temp;
			}
		}
		for (int k = 0; k < rou.length; k++) {
			System.out.println(rou[k]);
		}
	}

	void displayData() {
		System.out.println("==========TOTAL DATA IN THE FILE==============");
		for (int k = 0; k < recordCount(); k++) {
			System.out.println(routes[k]);
		}
	}
	
	
	void intermediateCities(String fromCity, String toCity)
	{
		System.out.println("==========ROUTE WITH INTERMEDIATE STATIONS==============");
		for(int i=0;i<routes.length;i++)
		{
			String fCity = routes[i].getFrom();
			String tCity = routes[i].getTo();
			if(fromCity.equals(fCity))
			{
				if(toCity.equals(tCity))
				{
					System.out.println(routes[i]);
				}
				else
				{
					for(int j=1;j<routes.length;j++)
					{
						if(tCity.equals(routes[j].getFrom()) && toCity.equals(routes[j].getTo()))
						{
							System.out.println(routes[i]);
							System.out.println(routes[j]);
						}
					}
				}
			}
		}
	}
	
	
	public static void main(String[] args) {
		String[] two = {"hyderabad","delhi"};
		RouteDetails rd = new RouteDetails();
		rd.storeRecords();
		//rd.displayData();
		rd.showDirectFlights("mumbai",two);
		rd.sortDirectFlights(two,"mumbai");
		rd.intermediateCities("mumbai", "delhi");
	}
}